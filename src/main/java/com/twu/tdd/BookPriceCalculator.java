package com.twu.tdd;

public class BookPriceCalculator {

    public Double calculatePrice(Integer[] input) {
        int type = 0;
        int bookCount = 0;
        for(Integer num:input){
            bookCount+=num;
            if(num>=1){
                type++;
            }
        }

        Double discountedMoney = type * 8.0 * calculateDiscount(type);
        Double notDiscountedMoney = (bookCount-type) * 8.0;
        return discountedMoney+notDiscountedMoney;
    }

    private Double calculateDiscount(int type) {
        Double discount = 1.0;
        switch (type){
            case 1:
                discount = 1.0;
                break;
            case 2:
                discount = 0.95;
                break;
            case 3:
                discount = 0.9;
                break;
            case 4:
                discount = 0.8;
                break;
            case 5:
                discount = 0.75;
                break;
                default:
                    discount = 1.0;
                    break;
        }
        return discount;
    }
}
