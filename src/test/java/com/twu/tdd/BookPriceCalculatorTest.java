package com.twu.tdd;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookPriceCalculatorTest {


    @Test
    void shouldReturn8EURWhenBuy1Book() {
        Map<Integer[],Double> bookPricePair = new HashMap<>();
        bookPricePair.put(new Integer[]{1,0,0,0,0},8.0);
        bookPricePair.put(new Integer[]{0,1,0,0,0},8.0);
        bookPricePair.put(new Integer[]{0,0,1,0,0},8.0);
        bookPricePair.put(new Integer[]{0,0,0,1,0},8.0);
        bookPricePair.put(new Integer[]{0,0,0,0,1},8.0);

        BookPriceCalculator bookPriceCalculator = new BookPriceCalculator();
        for(Map.Entry<Integer[],Double> entry:bookPricePair.entrySet()) {
            assertEquals(bookPriceCalculator.calculatePrice(entry.getKey()), entry.getValue());
        }
    }

    @Test
    void shouldReturn8EURWhenBuyDifferent2Book() {
        Map<Integer[],Double> bookPricePair = new HashMap<>();
        bookPricePair.put(new Integer[]{1,1,0,0,0},8.0*0.95*2);
        bookPricePair.put(new Integer[]{1,0,2,0,0},8.0*0.95*2+8);
        bookPricePair.put(new Integer[]{1,0,0,3,0},8.0*0.95*2+8*2);
        bookPricePair.put(new Integer[]{1,0,0,0,4},8.0*0.95*2+8*3);

        BookPriceCalculator bookPriceCalculator = new BookPriceCalculator();
        for(Map.Entry<Integer[],Double> entry:bookPricePair.entrySet()) {
            assertEquals(bookPriceCalculator.calculatePrice(entry.getKey()), entry.getValue());
        }
    }

    @Test
    void shouldReturn8EURWhenBuyDifferent3Book() {
        Map<Integer[],Double> bookPricePair = new HashMap<>();
        bookPricePair.put(new Integer[]{1,1,1,0,0},8.0*0.90*3);
        bookPricePair.put(new Integer[]{1,1,2,0,0},8.0*0.90*3+8);
        bookPricePair.put(new Integer[]{1,0,1,3,0},8.0*0.90*3+8*2);
        bookPricePair.put(new Integer[]{1,0,1,0,4},8.0*0.90*3+8*3);

        BookPriceCalculator bookPriceCalculator = new BookPriceCalculator();
        for(Map.Entry<Integer[],Double> entry:bookPricePair.entrySet()) {
            assertEquals(bookPriceCalculator.calculatePrice(entry.getKey()), entry.getValue());
        }
    }

    @Test
    void shouldReturn8EURWhenBuyDifferent4Book() {
        Map<Integer[],Double> bookPricePair = new HashMap<>();
        bookPricePair.put(new Integer[]{1,1,1,1,0},8.0*0.8*4);
        bookPricePair.put(new Integer[]{1,1,2,1,0},8.0*0.8*4+8);
        bookPricePair.put(new Integer[]{1,1,1,3,0},8.0*0.8*4+8*2);
        bookPricePair.put(new Integer[]{1,0,1,1,4},8.0*0.8*4+8*3);

        BookPriceCalculator bookPriceCalculator = new BookPriceCalculator();
        for(Map.Entry<Integer[],Double> entry:bookPricePair.entrySet()) {
            assertEquals(bookPriceCalculator.calculatePrice(entry.getKey()), entry.getValue());
        }
    }

    @Test
    void shouldReturn8EURWhenBuyDifferent5Book() {
        Map<Integer[],Double> bookPricePair = new HashMap<>();
        bookPricePair.put(new Integer[]{1,1,1,1,1},8.0*0.75*5);
        bookPricePair.put(new Integer[]{1,1,2,2,1},8.0*0.75*5+8*2);
        bookPricePair.put(new Integer[]{3,1,1,3,1},8.0*0.75*5+8*4);
        bookPricePair.put(new Integer[]{1,1,2,1,4},8.0*0.75*5+8*4);

        BookPriceCalculator bookPriceCalculator = new BookPriceCalculator();
        for(Map.Entry<Integer[],Double> entry:bookPricePair.entrySet()) {
            assertEquals(bookPriceCalculator.calculatePrice(entry.getKey()), entry.getValue());
        }
    }
}